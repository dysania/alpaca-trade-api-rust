pub const API_LIVE_URL: &str = "https://api.alpaca.markets";
pub const API_PAPER_URL: &str = "https://paper-api.alpaca.markets";
pub const MARKET_DATA_URL: &str = "https://data.alpaca.markets";
pub const POLYGON_URL: &str = "wss://socket.polygon.io/stocks";

pub mod env_vars {
    pub const API_KEY_ID: &str = "APCA_API_KEY_ID";
    pub const API_SECRET_KEY: &str = "APCA_API_SECRET_KEY";
}

pub mod api;
pub mod client;
pub mod data;
pub mod polygon;
mod serde;
