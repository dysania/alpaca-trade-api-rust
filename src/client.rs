use crate::api::account::AccountClient;
use crate::api::account_activities::AccountActivitiesClient;
use crate::api::account_config::AccountConfigClient;
use crate::api::assets::AssetsClient;
use crate::api::calendar::CalendarClient;
use crate::api::clock::ClockClient;
use crate::api::orders::OrdersClient;
use crate::api::positions::PositionsClient;
use crate::api::watchlist::WatchlistClient;
use crate::data::bars::BarsClient;
use crate::data::last_quote::LastQuoteClient;
use crate::data::last_trade::LastTradeClient;
use reqwest::Client as ReqClient;

use reqwest::header::{HeaderMap, HeaderValue};
use std::error::Error;
use std::rc::Rc;

pub struct Client {
    pub api: ApiClient,
    pub data: DataClient,
}

impl Client {
    pub fn new(api_key_id: &str, api_key_secret: &str) -> Result<Client, Box<dyn Error>> {
        let mut headers = HeaderMap::new();
        headers.insert("APCA-API-KEY-ID", HeaderValue::from_str(api_key_id)?);
        headers.insert(
            "APCA-API-SECRET-KEY",
            HeaderValue::from_str(api_key_secret)?,
        );
        let req_client = ReqClient::builder().default_headers(headers).build()?;

        // todo: change the URL based on config or whatever
        let client_helper = ClientHelper {
            req_client,
            api_base_url: crate::API_LIVE_URL.into(),
            data_base_url: crate::MARKET_DATA_URL.into(),
        };

        let client_helper = Rc::new(client_helper);

        Ok(Client {
            api: ApiClient {
                account: AccountClient::new(client_helper.clone()),
                account_activities: AccountActivitiesClient::new(client_helper.clone()),
                account_config: AccountConfigClient::new(client_helper.clone()),
                assets: AssetsClient::new(client_helper.clone()),
                calendar: CalendarClient::new(client_helper.clone()),
                clock: ClockClient::new(client_helper.clone()),
                orders: OrdersClient::new(client_helper.clone()),
                positions: PositionsClient::new(client_helper.clone()),
                watchlist: WatchlistClient::new(client_helper.clone()),
            },
            data: DataClient {
                bar: BarsClient::new(client_helper.clone()),
                last_quote: LastQuoteClient::new(client_helper.clone()),
                last_trade: LastTradeClient::new(client_helper.clone()),
            },
        })
    }
}

pub(in crate) struct ClientHelper {
    pub req_client: ReqClient,
    pub api_base_url: String,
    pub data_base_url: String,
}

pub struct DataClient {
    pub bar: BarsClient,
    pub last_quote: LastQuoteClient,
    pub last_trade: LastTradeClient,
}

pub struct ApiClient {
    pub account: AccountClient,
    pub account_activities: AccountActivitiesClient,
    pub account_config: AccountConfigClient,
    pub assets: AssetsClient,
    pub calendar: CalendarClient,
    pub clock: ClockClient,
    pub orders: OrdersClient,
    pub positions: PositionsClient,
    pub watchlist: WatchlistClient,
}
