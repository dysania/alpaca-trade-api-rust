use crate::client::ClientHelper;
use chrono::{DateTime, Utc};
use serde::Deserialize;
use std::error::Error;
use std::rc::Rc;

use chrono::serde::ts_nanoseconds::deserialize as from_nano_ts;

pub struct LastQuoteClient {
    client_helper: Rc<ClientHelper>,
}

impl LastQuoteClient {
    pub(in crate) fn new(client_helper: Rc<ClientHelper>) -> LastQuoteClient {
        LastQuoteClient { client_helper }
    }

    pub async fn get_last_quote(
        &self,
        symbol: String,
    ) -> Result<LastQuoteResponse, Box<dyn Error>> {
        let url = format!(
            "{}{}/{}",
            self.client_helper.api_base_url, "/v1/last_quote/stocks", symbol
        );
        let req = self.client_helper.req_client.get(&url);

        Ok(req.send().await?.json().await?)
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct LastQuoteResponse {
    pub status: String,
    pub symbol: String,
    pub last: LastQuote,
}

#[derive(Deserialize, Debug, Clone)]
pub struct LastQuote {
    #[serde(rename = "bidprice")]
    bid_price: f64,

    #[serde(rename = "bidsize")]
    bid_size: i64,

    #[serde(rename = "bidexchange")]
    bid_exchange: isize,

    #[serde(rename = "asksize")]
    ask_size: i64,

    #[serde(rename = "askexchange")]
    ask_exchange: isize,

    #[serde(rename = "askprice")]
    ask_price: f64,

    #[serde(deserialize_with = "from_nano_ts")]
    timestamp: DateTime<Utc>,
}
