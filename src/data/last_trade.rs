use crate::client::ClientHelper;
use chrono::{DateTime, Utc};
use serde::Deserialize;
use std::error::Error;
use std::rc::Rc;

use chrono::serde::ts_nanoseconds::deserialize as from_nano_ts;

pub struct LastTradeClient {
    client_helper: Rc<ClientHelper>,
}

impl LastTradeClient {
    pub(in crate) fn new(client_helper: Rc<ClientHelper>) -> Self {
        LastTradeClient { client_helper }
    }

    pub async fn get_last_trade(
        &self,
        symbol: String,
    ) -> Result<LastTradeResponse, Box<dyn Error>> {
        let url = format!(
            "{}{}/{}",
            self.client_helper.api_base_url, "/v1/last/stocks", symbol
        );
        let req = self.client_helper.req_client.get(&url);

        Ok(req.send().await?.json().await?)
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct LastTradeResponse {
    pub status: String,
    pub symbol: String,
    pub last: LastTrade,
}

#[derive(Deserialize, Debug, Clone)]
pub struct LastTrade {
    pub price: f64,
    pub size: i64,
    pub exchange: isize,
    pub cond1: isize,
    pub cond2: isize,
    pub cond3: isize,
    pub cond4: isize,

    #[serde(deserialize_with = "from_nano_ts")]
    pub timestamp: DateTime<Utc>,
}
