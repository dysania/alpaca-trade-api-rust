use crate::client::ClientHelper;
use crate::serde::*;
use chrono::{DateTime, Utc};
use derive_builder::Builder;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::error::Error;
use std::rc::Rc;

pub struct BarsClient {
    client_helper: Rc<ClientHelper>,
}

impl BarsClient {
    pub(in crate) fn new(client_helper: Rc<ClientHelper>) -> BarsClient {
        BarsClient { client_helper }
    }

    pub async fn get_bars(
        &self,
        timeframe: BarTimeframe,
        opt: GetBarsOptions,
    ) -> Result<HashMap<String, Vec<Bar>>, Box<dyn Error>> {
        let url = format!(
            "{}{}/{}",
            self.client_helper.data_base_url,
            "/v1/bars",
            match timeframe {
                BarTimeframe::Minute => "minute",
                BarTimeframe::FiveMin => "5Min",
                BarTimeframe::FifteenMin => "15Min",
                BarTimeframe::Day => "day",
            }
        );
        let req = self.client_helper.req_client.get(&url).query(&opt);

        Ok(req.send().await?.json().await?)
    }
}

#[derive(Serialize, Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct GetBarsOptions {
    #[serde(with = "string_vec_to_query_string")]
    pub symbols: Vec<String>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<isize>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub start: Option<DateTime<Utc>>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub end: Option<DateTime<Utc>>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub after: Option<DateTime<Utc>>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub until: Option<DateTime<Utc>>,
}

#[derive(Debug)]
pub enum BarTimeframe {
    Minute,
    FiveMin,
    FifteenMin,
    Day,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Bar {
    #[serde(rename = "t")]
    start_time: i64,

    #[serde(rename = "o")]
    open_price: f64,

    #[serde(rename = "h")]
    high_price: f64,

    #[serde(rename = "l")]
    low_price: f64,

    #[serde(rename = "c")]
    close_price: f64,

    #[serde(rename = "v")]
    volume: i64,
}
