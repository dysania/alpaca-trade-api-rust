use serde::Serializer;

use crate::polygon::stream::Channel;
pub fn channel_list_to_comma_string<S>(f: &Vec<Channel>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let s = f.iter().fold("".into(), |acc: String, next| {
        let next = match next {
            // todo: maybe this could be done by serializing the enum
            Channel::MinuteAggregate(s) => String::from("AM.") + &s.to_uppercase(),
            Channel::SecondAggregate(s) => String::from("A.") + &s.to_uppercase(),
            Channel::Quotes(s) => String::from("Q.") + &s.to_uppercase(),
            Channel::Trades(s) => String::from("T.") + &s.to_uppercase(),
        };
        acc + &next + ","
    });
    serializer.serialize_str(&s[..s.len() - 1])
}

// todo: Everything below should be refactored out of modules and be used as bare functions

/// Converts float strings to/from f64
/// Shamelessly "inspired" by the great [Serde Docs](https://serde.rs/custom-date-format.html)
pub mod string_to_f64 {
    use serde::{Deserialize, Deserializer, Serializer};

    pub fn deserialize<'de, D>(deserializer: D) -> Result<f64, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        s.parse::<f64>().map_err(serde::de::Error::custom)
    }

    pub fn serialize<S>(f: &f64, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = format!("{}", f);
        serializer.serialize_str(&s)
    }
}

pub mod string_vec_to_query_string {
    use serde::Serializer;

    pub fn serialize<S>(f: &Vec<String>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = f
            .iter()
            .fold("".into(), |acc: String, next| acc + next + ",");
        serializer.serialize_str(&s[..s.len() - 1])
    }
}

pub mod string_to_naivetime {
    use chrono::naive::NaiveTime;
    use serde::{Deserialize, Deserializer};

    pub fn deserialize<'de, D>(deserializer: D) -> Result<NaiveTime, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        NaiveTime::parse_from_str(&s, "%H:%M").map_err(serde::de::Error::custom)
    }
}

pub mod string_to_optional_f64 {
    use serde::{Deserialize, Deserializer};
    pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<f64>, D::Error>
    where
        D: Deserializer<'de>,
    {
        match Option::<String>::deserialize(deserializer)? {
            Some(s) => Some(s.parse::<f64>())
                .transpose()
                .map_err(serde::de::Error::custom),
            None => Ok(None),
        }
    }
}
