use alpaca_trade_api::polygon::stream::Channel;
use alpaca_trade_api::polygon::stream::Client;
use std::error::Error;
use tokio::stream::StreamExt;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    loop {
        let mut c = Client::new("AXXXXXXXXXXXXXXXXXXX").await.unwrap();
        c.subscribe(vec![Channel::SecondAggregate("*".into())])
            .await;
        while let Some(r) = c.next().await {
            dbg!(r);
        }
    }
}
