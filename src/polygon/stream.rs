use crate::serde::channel_list_to_comma_string;
use crate::POLYGON_URL;
use chrono::serde::ts_milliseconds::deserialize as from_ms_ts;
use chrono::{DateTime, Utc};
use futures_util::sink::Sink;
use futures_util::stream::{self, BoxStream, Stream};
use futures_util::task::{Context, Poll};
use futures_util::{SinkExt, StreamExt};
use serde::{Deserialize, Serialize};
use serde_json::from_str as from_json_string;
use serde_json::to_string as to_json_string;
// use std::collections::HashSet;
use std::error::Error;
use std::pin::Pin;
// use std::sync::{Arc, Mutex};
use tokio_tungstenite::connect_async;
use tokio_tungstenite::tungstenite::error::Error as TnError;
use tokio_tungstenite::tungstenite::Message;

pub type Symbol = String;
type BoxSink<T> = Pin<Box<dyn Sink<T, Error = TnError>>>;

pub struct Client<'a> {
    // subscriptions: Arc<Mutex<HashSet<Channel>>>,
    writer: BoxSink<Message>,
    reader: BoxStream<'a, Response>,
}

impl Client<'_> {
    pub async fn new(api_key_id: &str) -> Result<Client<'_>, Box<dyn Error>> {
        // connect
        let (ws, _) = connect_async(POLYGON_URL).await.unwrap();
        let (mut writer, mut reader) = ws.split();

        // authenticate
        let auth_req = Request::Authenticate(AuthenticateRequest {
            api_key: api_key_id.into(),
        });
        let auth_req = to_json_string(&auth_req).unwrap();
        writer.send(Message::Text(auth_req)).await.unwrap();
        writer.flush().await.unwrap();
        let mut auth_success = false;
        while auth_success != true {
            let msg = &reader.next().await.unwrap().unwrap();
            if msg.is_text() {
                let mut response: Vec<Response> = from_json_string(msg.to_text().unwrap()).unwrap();
                auth_success = match response.pop().unwrap() {
                    Response::Status(status) => status.status == StatusType::AuthSuccess,
                    _ => false,
                };
            }
        }

        let reader = reader
            .filter_map(|m| async {
                let m = m.unwrap();
                if !m.is_text() {
                    return None;
                }

                let mut responses: Vec<Response> =
                    from_json_string(&m.into_text().unwrap()).unwrap();

                responses.retain(|event| {
                    match event {
                        Response::Status(status) => {
                            match status.status {
                                StatusType::Success => {
                                    // dbg!(&status.message);
                                }
                                _ => {
                                    panic!(format!("{:?}", status));
                                }
                            };
                            false
                        }
                        _ => true,
                    }
                });

                Some(stream::iter(responses))
            })
            .flatten();

        Ok(Client {
            // subscriptions: Arc::default(),
            writer: Box::pin(writer),
            reader: reader.boxed(),
        })
    }

    pub async fn subscribe(&mut self, c: Vec<Channel>) {
        let req = Request::Subscribe(SubscribeRequest { channel: c });
        let req = to_json_string(&req).unwrap();
        self.writer.send(Message::Text(req)).await.unwrap();
        self.writer.flush().await.unwrap();
    }
}

impl Stream for Client<'_> {
    type Item = Response;
    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        self.reader.poll_next_unpin(cx)
    }
}

#[derive(Serialize, Debug)]
pub enum Channel {
    Trades(Symbol),
    Quotes(Symbol),
    MinuteAggregate(Symbol),
    SecondAggregate(Symbol),
}

#[derive(Serialize, Debug)]
#[serde(tag = "action", rename_all = "lowercase")]
pub enum Request {
    Subscribe(SubscribeRequest),
    Unsubscribe(UnsubscribeRequest),

    #[serde(rename = "auth")]
    Authenticate(AuthenticateRequest),
}

#[derive(Serialize, Debug)]
pub struct AuthenticateRequest {
    #[serde(rename = "params")]
    api_key: String,
}

#[derive(Serialize, Debug)]
pub struct UnsubscribeRequest {
    #[serde(serialize_with = "channel_list_to_comma_string")]
    #[serde(rename = "params")]
    channel: Vec<Channel>,
}

#[derive(Serialize, Debug)]
pub struct SubscribeRequest {
    #[serde(serialize_with = "channel_list_to_comma_string")]
    #[serde(rename = "params")]
    channel: Vec<Channel>,
}

#[derive(Deserialize, Debug)]
#[serde(tag = "ev", rename_all = "lowercase")]
pub enum Response {
    Status(Status),

    #[serde(rename = "T")]
    Trade(Trade),

    #[serde(rename = "Q")]
    Quote(Quote),

    #[serde(rename = "A")]
    SecondAggregate(Aggregate),

    #[serde(rename = "AM")]
    MinuteAggregate(Aggregate),
}

#[derive(Deserialize, Debug)]
pub struct Status {
    status: StatusType,
    message: String,
}

#[derive(Deserialize, Debug, PartialEq)]
#[serde(rename_all = "snake_case")]
pub enum StatusType {
    /// Immediately returned on socket connection
    Connected,

    /// Returned on successful auth. The docs say "success" for this and
    /// other message, but upon testing it really returned "auth_success" 🤷
    AuthSuccess,

    /// Successful actions, such as a subscription
    Success,

    /// On auth failure (such as bad token)
    AuthFailed,

    /// Auth did not happen in time
    AuthTimeout,

    /// Not authorized
    AuthRequired,

    // Maximum number of connections exceeded
    MaxConnections,
}

#[derive(Deserialize, Debug)]
pub struct Trade {
    #[serde(rename = "sym")]
    symbol: String,

    #[serde(rename = "x")]
    exchange_id: isize,

    #[serde(rename = "i")]
    trade_id: String,

    #[serde(rename = "z")]
    tape: isize,

    #[serde(rename = "p")]
    price: f64,

    #[serde(rename = "s")]
    trade_size: i64,

    #[serde(rename = "c")]
    // Either Option<> or #[serde(default)] to use empty vector
    conditions: Option<Vec<isize>>,

    #[serde(rename = "t")]
    #[serde(deserialize_with = "from_ms_ts")]
    timestamp: DateTime<Utc>,
}

#[derive(Deserialize, Debug)]
pub struct Quote {
    #[serde(rename = "sym")]
    symbol: String,

    #[serde(rename = "bx")]
    bid_exchange_id: Option<isize>,

    #[serde(rename = "bs")]
    bid_size: Option<i64>,

    #[serde(rename = "bp")]
    bid_price: Option<f64>,

    #[serde(rename = "ax")]
    ask_exchange_id: Option<isize>,

    #[serde(rename = "ap")]
    ask_price: Option<f64>,

    #[serde(rename = "as")]
    ask_size: Option<i64>,

    #[serde(rename = "c")]
    quote_condition: isize,

    #[serde(rename = "t")]
    #[serde(deserialize_with = "from_ms_ts")]
    timestamp: DateTime<Utc>,
}

#[derive(Deserialize, Debug)]
pub struct Aggregate {
    #[serde(rename = "sym")]
    symbol: String,

    #[serde(rename = "v")]
    volume: i64,

    #[serde(rename = "av")]
    accumulated_volume: i64,

    #[serde(rename = "op")]
    day_open_price: Option<f64>,

    #[serde(rename = "vw")]
    volume_weighted_price: f64,

    #[serde(rename = "o")]
    tick_open_price: f64,

    #[serde(rename = "c")]
    tick_close_price: f64,

    #[serde(rename = "h")]
    tick_high_price: f64,

    #[serde(rename = "l")]
    tick_low_price: f64,

    #[serde(rename = "a")]
    tick_average_price: f64, // Tick Volume Weighted Average Price

    #[serde(rename = "s")]
    #[serde(deserialize_with = "from_ms_ts")]
    tick_start_time: DateTime<Utc>,

    #[serde(rename = "e")]
    #[serde(deserialize_with = "from_ms_ts")]
    tick_end_time: DateTime<Utc>,
}
