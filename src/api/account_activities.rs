use crate::client::ClientHelper;

use super::orders::OrderSide;

use crate::serde::string_to_f64;
use chrono::{DateTime, Utc};
use derive_builder::Builder;

use serde::{Deserialize, Serialize};
use std::error::Error;
use uuid::Uuid;

use std::rc::Rc;
pub struct AccountActivitiesClient {
    client_helper: Rc<ClientHelper>,
}

impl AccountActivitiesClient {
    pub(in crate) fn new(client_helper: Rc<ClientHelper>) -> AccountActivitiesClient {
        AccountActivitiesClient { client_helper }
    }

    pub async fn get_account_activities(
        &self,
        activity_type: ActivityType,
        _opt: GetAccountActivitiesOptions,
    ) -> Result<Vec<Activity>, Box<dyn Error>> {
        let type_string = serde_json::to_string(&activity_type).unwrap();
        let url = format!(
            "{}{}/{}",
            self.client_helper.api_base_url,
            "/v2/account/activities",
            &type_string[1..type_string.len() - 1]
        );
        let res = self.client_helper.req_client.get(&url).send().await?;

        let res = res.text().await?;
        println!("{}", res);

        todo!()
        // this one is hard. Here's an actual (censored) response I got for a CashReceipt req:
        //         [
        //     {
        //         "id": "20190925000000000::3f8d4d91-97bd-441c-8ae5-ed01a1bbf45b",
        //         "activity_type": "CSR",
        //         "date": "2019-09-25",
        //         "net_amount": "20",
        //         "description": "CHASE BANK"
        //     }
        // ]
    }
}
#[derive(Deserialize, Serialize, Debug, Default, Builder, Clone)]
#[builder(setter(strip_option))]
#[builder(default)]
pub struct GetAccountActivitiesOptions {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub date: Option<DateTime<Utc>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub until: Option<DateTime<Utc>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub after: Option<DateTime<Utc>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub direction: Option<super::orders::Ordering>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub page_size: Option<usize>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub page_token: Option<String>,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum Activity {
    TradeActivity(TradeActivity),
    NonTradeActivity(NonTradeActivity),
}

#[derive(Deserialize, Debug, Clone)]
pub struct TradeActivity {
    pub activity_type: ActivityType,
    pub id: String,
    pub side: OrderSide,
    pub symbol: String,
    transaction_time: DateTime<Utc>,
    order_id: Uuid,

    #[serde(rename = "type")]
    order_type: TradeActivityType,

    #[serde(with = "string_to_f64")]
    pub cum_qty: f64,

    #[serde(with = "string_to_f64")]
    pub leaves_qty: f64,

    #[serde(with = "string_to_f64")]
    pub price: f64,

    #[serde(with = "string_to_f64")]
    pub qty: f64,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum TradeActivityType {
    Fill,
    PartialFill,
}

#[derive(Deserialize, Debug, Clone)]
pub struct NonTradeActivity {
    pub activity_type: ActivityType,
    pub id: String,
    pub date: DateTime<Utc>,
    pub symbol: String,

    #[serde(with = "string_to_f64")]
    pub net_amount: f64,

    #[serde(with = "string_to_f64")]
    pub qty: f64,

    #[serde(with = "string_to_f64")]
    pub per_share_amount: f64,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "UPPERCASE")]
pub enum ActivityType {
    Fill,
    Misc,

    #[serde(rename = "SSP")]
    StockSplit,

    #[serde(rename = "SSO")]
    StockSpinoff,

    #[serde(rename = "SC")]
    SymbolChange,

    /// Reorg CA
    #[serde(rename = "REORG")]
    ReorgCa,

    #[serde(rename = "PTR")]
    PassThruRebate,

    #[serde(rename = "PTC")]
    PassThruCharge,

    #[serde(rename = "OPXRC")]
    OptionExercise,

    #[serde(rename = "OPEXP")]
    OptionExpiration,

    #[serde(rename = "OPASN")]
    OptionAssignment,

    #[serde(rename = "NC")]
    NameChange,

    /// Merger/Acquisition
    #[serde(rename = "MA")]
    MergerAcquisition,

    #[serde(rename = "JNLS")]
    JournalEntryStock,

    #[serde(rename = "JNLC")]
    JournalEntryCash,

    #[serde(rename = "JNL")]
    JournalEntry,

    #[serde(rename = "INTTW")]
    InterestAdjustedTefraWithheld,

    #[serde(rename = "INTNRA")]
    InterestAdjustedNraWithheld,

    #[serde(rename = "INT")]
    Interest,

    #[serde(rename = "DIVTXEX")]
    DividendTaxExempt,

    #[serde(rename = "DIVTW")]
    DividendAdjustedTefraWithheld,

    #[serde(rename = "DIVROC")]
    DividendReturnOfCapital,

    #[serde(rename = "DIVNRA")]
    DividendAdjustedNraWithheld,

    #[serde(rename = "DIVFT")]
    DividendAdjustedForeignTaxWithheld,

    #[serde(rename = "DIVFEE")]
    DividendFee,

    #[serde(rename = "DIVCGS")]
    DividendCapitalGainShortTerm,

    #[serde(rename = "DIVCGL")]
    DividendCapitalGainLongTerm,

    #[serde(rename = "DIV")]
    Dividends,

    #[serde(rename = "CSR")]
    CashReceipt,

    #[serde(rename = "CSD")]
    CashDisbursement,

    #[serde(rename = "ACATC")]
    AcatCash,

    #[serde(rename = "ACATS")]
    AcatSecurities,

    #[serde(rename = "TRANS")]
    Transactions,
}
