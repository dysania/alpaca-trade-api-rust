use crate::client::ClientHelper;
use chrono::{DateTime, Utc};
use derive_builder::Builder;

use serde::{Deserialize, Serialize};
use std::error::Error;

use std::rc::Rc;
use uuid::Uuid;

pub struct WatchlistClient {
    client_helper: Rc<ClientHelper>,
}

impl WatchlistClient {
    pub(in crate) fn new(client_helper: Rc<ClientHelper>) -> WatchlistClient {
        WatchlistClient { client_helper }
    }

    pub async fn get_watchlists(&self) -> Result<Vec<Watchlist>, Box<dyn Error>> {
        let url = format!("{}{}", self.client_helper.api_base_url, "/v2/watchlists");
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn get_watchlist(&self, list_id: Uuid) -> Result<Watchlist, Box<dyn Error>> {
        let url = format!(
            "{}{}{}",
            self.client_helper.api_base_url, "/v2/watchlists/", list_id
        );
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn create_watchlist(
        &self,
        req: CreateWatchlistRequest,
    ) -> Result<Vec<Watchlist>, Box<dyn Error>> {
        let url = format!("{}{}", self.client_helper.api_base_url, "/v2/orders");
        Ok(self
            .client_helper
            .req_client
            .post(&url)
            .json(&req)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn update_watchlist(
        &self,
        list_id: Uuid,
        req: CreateWatchlistRequest,
    ) -> Result<Vec<Watchlist>, Box<dyn Error>> {
        let url = format!(
            "{}{}{}",
            self.client_helper.api_base_url, "/v2/watchlists/", list_id
        );
        Ok(self
            .client_helper
            .req_client
            .put(&url)
            .json(&req)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn add_asset_to_watchlist(
        &self,
        list_id: Uuid,
        symbol: String,
    ) -> Result<Vec<Watchlist>, Box<dyn Error>> {
        let req = AddAssetRequestBuilder::default()
            .symbol(symbol)
            .build()
            .unwrap();
        let url = format!(
            "{}{}{}",
            self.client_helper.api_base_url, "/v2/watchlists/", list_id
        );
        Ok(self
            .client_helper
            .req_client
            .post(&url)
            .json(&req)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn remove_asset_from_watchlist(
        &self,
        list_id: Uuid,
        symbol: String,
    ) -> Result<(), Box<dyn Error>> {
        let url = format!(
            "{}{}{}/{}",
            self.client_helper.api_base_url, "/v2/watchlists/", list_id, symbol
        );
        self.client_helper.req_client.delete(&url).send().await?;
        Ok(())
    }

    pub async fn delete_watchlist(&self, list_id: Uuid) -> Result<(), Box<dyn Error>> {
        let url = format!(
            "{}{}{}",
            self.client_helper.api_base_url, "/v2/watchlists/", list_id
        );
        self.client_helper.req_client.delete(&url).send().await?;
        Ok(())
    }

    // todo: lists can also be access by name
    // https://alpaca.markets/docs/api-documentation/api-v2/watchlist/#endpoints-for-watchlist-name
}
#[derive(Deserialize, Serialize, Debug, Default, Builder, Clone)]
pub struct AddAssetRequest {
    symbol: String,
}

#[derive(Deserialize, Serialize, Debug, Default, Builder, Clone)]
pub struct CreateWatchlistRequest {
    name: String,
    symbols: Vec<String>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Watchlist {
    account_id: Uuid,
    assets: Vec<super::assets::Asset>,
    created_at: DateTime<Utc>,
    id: Uuid,
    name: String,
    updated_at: DateTime<Utc>,
}
