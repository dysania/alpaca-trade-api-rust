use crate::client::ClientHelper;

use crate::serde::string_to_naivetime;
use chrono::naive::{NaiveDate, NaiveTime};
use derive_builder::Builder;

use serde::{Deserialize, Serialize};
use std::error::Error;

use std::rc::Rc;

pub struct CalendarClient {
    client_helper: Rc<ClientHelper>,
}

impl CalendarClient {
    pub(in crate) fn new(client_helper: Rc<ClientHelper>) -> CalendarClient {
        CalendarClient { client_helper }
    }

    pub async fn get_calendar(
        &self,
        _opt: Option<GetCalendarOptions>,
    ) -> Result<Vec<Calendar>, Box<dyn Error>> {
        let url = format!("{}{}", self.client_helper.api_base_url, "/v2/calendar");
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .send()
            .await?
            .json()
            .await?)
    }
}

#[derive(Deserialize, Serialize, Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct GetCalendarOptions {
    pub start: NaiveDate,
    pub end: NaiveDate,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Calendar {
    pub date: NaiveDate,

    #[serde(with = "string_to_naivetime")]
    pub open: NaiveTime,
    #[serde(with = "string_to_naivetime")]
    pub close: NaiveTime,
}
