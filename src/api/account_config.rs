use crate::client::ClientHelper;
use derive_builder::Builder;

use serde::{Deserialize, Serialize};
use std::error::Error;

use std::rc::Rc;

pub struct AccountConfigClient {
    client_helper: Rc<ClientHelper>,
}

impl AccountConfigClient {
    pub(in crate) fn new(client_helper: Rc<ClientHelper>) -> AccountConfigClient {
        AccountConfigClient { client_helper }
    }

    pub async fn get_account_config(&self) -> Result<AccountConfig, Box<dyn Error>> {
        let url = format!(
            "{}{}",
            self.client_helper.api_base_url, "/v2/account/configurations"
        );
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn update_account_config(
        &self,
        opt: AccountOptions,
    ) -> Result<AccountConfig, Box<dyn Error>> {
        let url = format!(
            "{}{}",
            self.client_helper.api_base_url, "/v2/account/configurations"
        );
        Ok(self
            .client_helper
            .req_client
            .patch(&url)
            .json(&opt)
            .send()
            .await?
            .json()
            .await?)
    }
}

// todo: implement into betwenn Options and Config ⇓

#[derive(Deserialize, Serialize, Debug, Default, Builder, Clone)]
#[builder(setter(strip_option))]
#[builder(default)]
pub struct AccountOptions {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub dtbp_check: Option<DtbpCheck>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub trade_confirm_email: Option<TradeConfirmEmail>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub suspend_trade: Option<bool>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub no_shorting: Option<bool>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct AccountConfig {
    pub dtbp_check: DtbpCheck,
    pub trade_confirm_email: TradeConfirmEmail,
    pub suspend_trade: bool,
    pub no_shorting: bool,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "lowercase")]
pub enum DtbpCheck {
    Both,
    Entry,
    Exit,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "lowercase")]
pub enum TradeConfirmEmail {
    All,
    None,
}
