use crate::client::ClientHelper;
use chrono::{DateTime, Utc};

use serde::Deserialize;
use std::error::Error;
use std::rc::Rc;

pub struct ClockClient {
    client_helper: Rc<ClientHelper>,
}

impl ClockClient {
    pub(in crate) fn new(client_helper: Rc<ClientHelper>) -> ClockClient {
        ClockClient { client_helper }
    }

    pub async fn get_clock(&self) -> Result<Clock, Box<dyn Error>> {
        let url = format!("{}{}", self.client_helper.api_base_url, "/v2/clock");
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .send()
            .await?
            .json()
            .await?)
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct Clock {
    pub timestamp: DateTime<Utc>,
    pub is_open: bool,
    pub next_open: DateTime<Utc>,
    pub next_close: DateTime<Utc>,
}
