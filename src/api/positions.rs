use crate::client::ClientHelper;
use crate::serde::string_to_f64;

use serde::{Deserialize, Serialize};
use std::error::Error;
use std::rc::Rc;
use uuid::Uuid;

pub struct PositionsClient {
    client_helper: Rc<ClientHelper>,
}

impl PositionsClient {
    pub(in crate) fn new(client_helper: Rc<ClientHelper>) -> PositionsClient {
        PositionsClient { client_helper }
    }

    // todo: the vast majority of these are untested
    pub async fn get_positions(&self) -> Result<Vec<Position>, Box<dyn Error>> {
        // todo: this is untested. I have no alpaca positions
        let url = format!("{}{}", self.client_helper.api_base_url, "/v2/positions");
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn get_position(&self, sym: String) -> Result<Position, Box<dyn Error>> {
        let url = format!(
            "{}{}{}",
            self.client_helper.api_base_url, "/v2/positions/", sym
        );
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn close_all_positions(&self) -> Result<(), Box<dyn Error>> {
        let url = format!("{}{}", self.client_helper.api_base_url, "/v2/positions");
        self.client_helper.req_client.delete(&url).send().await?;
        Ok(())
    }

    pub async fn close_position(
        &self,
        sym: String,
    ) -> Result<super::orders::Order, Box<dyn Error>> {
        let url = format!(
            "{}{}{}",
            self.client_helper.api_base_url, "/v2/positions/", sym
        );
        Ok(self
            .client_helper
            .req_client
            .delete(&url)
            .send()
            .await?
            .json()
            .await?)
    }
}
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Position {
    pub asset_id: Uuid,
    pub symbol: String,
    pub exchange: String,
    pub asset_class: String,
    pub side: PositionSide,

    #[serde(with = "string_to_f64")]
    pub avg_entry_price: f64,

    #[serde(with = "string_to_f64")]
    pub market_value: f64,

    #[serde(with = "string_to_f64")]
    pub qty: f64,

    /// Total cost basis in dollar
    #[serde(with = "string_to_f64")]
    pub cost_basis: f64,

    /// Unrealized profit/loss in dollars
    #[serde(with = "string_to_f64")]
    pub unrealized_pl: f64,

    /// Unrealized profit/loss percent (by a factor of 1)
    #[serde(with = "string_to_f64")]
    pub unrealized_plpc: f64,

    /// Unrealized profit/loss in dollars for the day
    #[serde(with = "string_to_f64")]
    pub unrealized_intraday_pl: f64,

    /// Unrealized profit/loss percent (by a factor of 1)
    #[serde(with = "string_to_f64")]
    pub unrealized_intraday_plpc: f64,

    /// Current asset price per share
    #[serde(with = "string_to_f64")]
    pub current_price: f64,

    /// Last day’s asset price per share based on the closing value of the last trading day
    #[serde(with = "string_to_f64")]
    pub lastday_price: f64,

    /// Percent change from last day price (by a factor of 1)
    #[serde(with = "string_to_f64")]
    pub change_today: f64,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "lowercase")]
pub enum PositionSide {
    Long,
    // https://alpaca.markets/docs/api-documentation/api-v2/positions/#position-entity
    // todo: this is unconfirmed. They didn't freaking document it with all its possibilities
    Short, // <-
}
