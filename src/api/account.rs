use crate::serde::string_to_f64;
use chrono::{DateTime, Utc};

use crate::client::ClientHelper;
use serde::{Deserialize, Serialize};
use std::error::Error;

use std::rc::Rc;
use uuid::Uuid;

pub struct AccountClient {
    client_helper: Rc<ClientHelper>,
}

impl AccountClient {
    pub(in crate) fn new(client_helper: Rc<ClientHelper>) -> AccountClient {
        AccountClient { client_helper }
    }

    /// [[GET] Get the account](https://alpaca.markets/docs/api-documentation/api-v2/account/#get-the-account)
    pub async fn get_account(&self) -> Result<Account, Box<dyn Error>> {
        let url = format!("{}{}", self.client_helper.api_base_url, "/v2/account");
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .send()
            .await?
            .json()
            .await?)
    }
}

/// [Account Entity](https://alpaca.markets/docs/api-documentation/api-v2/account/#account-entity)
#[derive(Deserialize, Serialize, Debug)]
pub struct Account {
    pub account_blocked: bool,
    pub account_number: String,
    pub status: AccountStatus,
    pub created_at: DateTime<Utc>,
    pub currency: String,
    pub daytrade_count: isize,
    pub id: Uuid,
    pub pattern_day_trader: bool,
    pub shorting_enabled: bool,
    pub trade_suspended_by_user: bool,
    pub trading_blocked: bool,
    pub transfers_blocked: bool,

    // todo: find a better way to do this!
    // it should be possible to have serde try this automatically
    #[serde(with = "string_to_f64")]
    pub equity: f64,

    #[serde(rename = "sma")]
    #[serde(with = "string_to_f64")]
    pub special_memorandum_account: f64,

    #[serde(with = "string_to_f64")]
    pub short_market_value: f64,

    #[serde(with = "string_to_f64")]
    pub multiplier: f64,

    #[serde(with = "string_to_f64")]
    pub long_market_value: f64,

    #[serde(with = "string_to_f64")]
    pub maintenance_margin: f64,

    #[serde(with = "string_to_f64")]
    pub portfolio_value: f64,

    #[serde(with = "string_to_f64")]
    #[serde(rename = "regt_buying_power")]
    pub regulation_t_buying_power: f64,

    #[serde(with = "string_to_f64")]
    pub last_maintenance_margin: f64,

    #[serde(with = "string_to_f64")]
    pub last_equity: f64,

    #[serde(with = "string_to_f64")]
    pub initial_margin: f64,

    #[serde(with = "string_to_f64")]
    pub daytrading_buying_power: f64,

    #[serde(with = "string_to_f64")]
    pub cash: f64,

    #[serde(with = "string_to_f64")]
    pub buying_power: f64,
}

/// [Account Status](https://alpaca.markets/docs/api-documentation/api-v2/account/#account-status)
#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum AccountStatus {
    Onboarding,
    SubmissionFailed,
    Submitted,
    AccountUpdated,
    ApprovalPending,
    Active,
    Rejected,
}
