use crate::client::ClientHelper;
use derive_builder::Builder;

use serde::{Deserialize, Serialize};
use std::error::Error;
use std::rc::Rc;
use uuid::Uuid;

pub struct AssetsClient {
    client_helper: Rc<ClientHelper>,
}

impl AssetsClient {
    pub(in crate) fn new(client_helper: Rc<ClientHelper>) -> AssetsClient {
        AssetsClient { client_helper }
    }

    pub async fn get_assets(&self, opt: GetAssetsOptions) -> Result<Vec<Asset>, Box<dyn Error>> {
        let url = format!("{}{}", self.client_helper.api_base_url, "/v2/assets");
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .query(&opt)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn get_asset_by_id(&self, asset_id: Uuid) -> Result<Asset, Box<dyn Error>> {
        let url = format!(
            "{}{}{}",
            self.client_helper.api_base_url, "/v2/assets/", asset_id
        );
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn get_asset_by_symbol(&self, sym: String) -> Result<Asset, Box<dyn Error>> {
        let url = format!(
            "{}{}{}",
            self.client_helper.api_base_url, "/v2/assets/", sym
        );
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .send()
            .await?
            .json()
            .await?)
    }
}

#[derive(Deserialize, Serialize, Debug, Default, Builder, Clone)]
#[builder(setter(strip_option))]
#[builder(default)]
pub struct GetAssetsOptions {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<AssetStatus>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub asset_class: Option<String>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Asset {
    pub id: Uuid,
    pub class: String,
    pub exchange: String,
    pub symbol: String,
    pub status: AssetStatus,
    pub tradable: bool,
    pub marginable: bool,
    pub shortable: bool,
    pub easy_to_borrow: bool,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum AssetStatus {
    Active,
    Inactive,
}
