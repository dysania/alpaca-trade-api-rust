use crate::client::ClientHelper;
use crate::serde::*;
use chrono::{DateTime, Utc};
use derive_builder::Builder;

use serde::{Deserialize, Serialize};
use std::error::Error;
use uuid::Uuid;

use std::rc::Rc;

pub struct OrdersClient {
    client_helper: Rc<ClientHelper>,
}

impl OrdersClient {
    pub(in crate) fn new(client_helper: Rc<ClientHelper>) -> OrdersClient {
        OrdersClient { client_helper }
    }

    // [Get Orders](get-a-list-of-orders)
    pub async fn get_orders(&self, opt: GetOrdersOptions) -> Result<Vec<Order>, Box<dyn Error>> {
        if let Some(limit) = opt.limit {
            if limit > 500 {
                panic!();
            }
        }

        let url = format!("{}{}", self.client_helper.api_base_url, "/v2/orders");
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn post_order(&self, req: OrderRequest) -> Result<Order, Box<dyn Error>> {
        let url = format!("{}{}", self.client_helper.api_base_url, "/v2/orders");
        Ok(self
            .client_helper
            .req_client
            .post(&url)
            .json(&req)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn get_order(&self, order_id: Uuid, nested: bool) -> Result<Order, Box<dyn Error>> {
        let url = format!(
            "{}{}{}",
            self.client_helper.api_base_url, "/v2/orders/", order_id
        );
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .query(&[("nested", nested)])
            .send()
            .await?
            .json()
            .await?)
    }

    /// The official docs are incomplete for this API
    ///
    /// Implementation was gleamed from the [Python SDK](https://github.com/alpacahq/alpaca-trade-api-python/blob/a61bb7816b0b717a41265a365972afd22946e44b/alpaca_trade_api/rest.py#L307-L313)
    pub async fn get_order_by_client_id(&self, id: String) -> Result<Order, Box<dyn Error>> {
        let url = format!(
            "{}{}",
            self.client_helper.api_base_url, "/v2/orders:by_client_order_id"
        );
        Ok(self
            .client_helper
            .req_client
            .get(&url)
            .query(&[("client_order_id", id)])
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn replace_order(
        &self,
        order_id: Uuid,
        opt: OrderReplaceRequest,
    ) -> Result<Order, Box<dyn Error>> {
        // todo: this is untested, it currently failes with 422
        // this is odd because I checked the python code. It's not complicated.
        // https://github.com/alpacahq/alpaca-trade-api-python/blob/a61bb7816b0b717a41265a365972afd22946e44b/alpaca_trade_api/rest.py#L321-L349
        let url = format!(
            "{}{}{}",
            self.client_helper.api_base_url, "/v2/orders/", order_id
        );
        let res = self
            .client_helper
            .req_client
            .patch(&url)
            .json(&opt)
            .send()
            .await?;
        println!("{:#?}", serde_json::to_string(&opt)?);

        println!("{:#?}", res);
        Ok(res.json().await?)
    }

    pub async fn cancel_all_orders(&self) -> Result<Vec<CancelAllResponse>, Box<dyn Error>> {
        let url = format!("{}{}", self.client_helper.api_base_url, "/v2/orders");
        Ok(self
            .client_helper
            .req_client
            .delete(&url)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn cancel_order(&self, order_id: Uuid) -> Result<(), Box<dyn Error>> {
        let url = format!(
            "{}{}{}",
            self.client_helper.api_base_url, "/v2/orders/", order_id
        );
        self.client_helper.req_client.delete(&url).send().await?;
        Ok(())
    }
}
#[derive(Deserialize, Debug)]
pub struct CancelAllResponse {
    pub id: Uuid,
    pub status: isize,
    pub body: Order,
}

#[derive(Serialize, Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct OrderReplaceRequest {
    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub time_in_force: Option<OrderTimeInForce>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub qty: Option<f64>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit_price: Option<f64>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub stop_price: Option<f64>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub client_order_id: Option<String>,
}

#[derive(Serialize, Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct OrderRequest {
    // todo: rename to PostOrderRequest
    pub symbol: String,
    pub qty: f64,
    pub side: OrderSide,
    pub time_in_force: OrderTimeInForce,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub take_profit: Option<OrderRequestTakeProfit>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub stop_loss: Option<OrderRequestStopLoss>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub client_order_id: Option<String>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub extended_hours: Option<bool>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit_price: Option<f64>,

    #[builder(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub stop_price: Option<f64>,

    #[serde(rename = "type")] // type is a keyword
    pub order_type: OrderType,
}

#[derive(Serialize, Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct OrderRequestTakeProfit {
    pub limit_price: f64,
}

#[derive(Serialize, Debug, Builder, Clone)]
#[builder(setter(strip_option))]
pub struct OrderRequestStopLoss {
    pub stop_price: f64,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit_price: Option<f64>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "lowercase")]
pub enum OrderClass {
    Simple,
    Bracket,

    #[serde(rename = "oto")]
    OneTriggersOther,

    #[serde(rename = "oco")]
    OneCancelsOther,
}

#[derive(Deserialize, Serialize, Debug, Default, Builder, Clone)]
#[builder(setter(strip_option))]
#[builder(default)]
pub struct GetOrdersOptions {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<GetOrdersOptionStatus>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub limit: Option<usize>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub after: Option<DateTime<Utc>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub until: Option<DateTime<Utc>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub direction: Option<Ordering>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub nested: Option<bool>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum GetOrdersOptionStatus {
    Open,
    Closed,
    All,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub enum Ordering {
    #[serde(rename = "asc")]
    Ascending,

    #[serde(rename = "desc")]
    Descending,
}

#[derive(Deserialize, Debug)]
pub struct Order {
    pub id: Uuid,
    pub client_order_id: Uuid,
    pub created_at: DateTime<Utc>,
    pub updated_at: Option<DateTime<Utc>>,
    pub submitted_at: Option<DateTime<Utc>>,
    pub filled_at: Option<DateTime<Utc>>,
    pub expired_at: Option<DateTime<Utc>>,
    pub canceled_at: Option<DateTime<Utc>>,
    pub failed_at: Option<DateTime<Utc>>,
    pub replaced_at: Option<DateTime<Utc>>,
    pub replaced_by: Option<String>,
    pub replaces: Option<String>,
    pub asset_id: String,
    pub symbol: String,
    pub asset_class: String,
    pub side: OrderSide,
    pub time_in_force: OrderTimeInForce,
    pub status: OrderStatus,
    pub extended_hours: bool,
    pub legs: Option<Vec<Order>>,

    #[serde(rename = "type")] // type is a keyword
    pub order_type: OrderType,

    #[serde(with = "string_to_optional_f64")]
    pub limit_price: Option<f64>,

    #[serde(with = "string_to_optional_f64")]
    pub stop_price: Option<f64>,

    #[serde(with = "string_to_optional_f64")]
    pub filled_avg_price: Option<f64>,

    #[serde(with = "string_to_f64")]
    pub qty: f64,

    #[serde(with = "string_to_f64")]
    pub filled_qty: f64,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum OrderType {
    Market,
    Limit,
    Stop,
    StopLimit,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum OrderSide {
    Buy,
    Sell,
}

/// [Time in Force](https://alpaca.markets/docs/trading-on-alpaca/orders/#time-in-force)
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum OrderTimeInForce {
    Day,
    Opg,
    Cls,

    #[serde(rename = "gtc")]
    GoodTilCanceled,

    #[serde(rename = "ioc")]
    ImmediateOrCancel,

    #[serde(rename = "fok")]
    ForkOrKill,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "snake_case")]
pub enum OrderStatus {
    New,
    PartiallyFilled,
    Filled,
    DoneForDay,
    Canceled,
    Expired,
    Replaced,
    PendingCancel,
    PendingReplace,
    Accepted,
    PendingNew,
    AcceptedForBidding,
    Stopped,
    Rejected,
    Suspended,
    Calculated,
}
